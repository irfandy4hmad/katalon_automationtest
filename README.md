# Katalon_AutomationTest
Automation Test

site use: https://katalon-demo-cura.herokuapp.com/

Test Cases to write:
1. Login successfully.
2. Book an appointment successfully.
    - Check the checkbox for Apply for hospital readmission
    - Select radio button, Medicaid
    - Enter the comment, This is for testing purposes only.
3. Logout successfully.
